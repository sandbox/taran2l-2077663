(function() {

  CKEDITOR.on('dialogDefinition', function(ev) {
    // Take the dialog name and its definition from the event
    // data.
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    if (dialogName === 'image') {
      var IMAGE = 1,
              LINK = 2,
              PREVIEW = 4,
              CLEANUP = 8,
              regexGetSize = /^\s*(\d+)((px)|\%)?\s*$/i,
              regexGetSizeOrEmpty = /(^\s*(\d+)((px)|\%)?\s*$)|^$/i,
              pxLengthRegex = /^\d+px$/;

      // Get a reference to the "Info" (main) tab.
      var infoTab = dialogDefinition.getContents('info');
      // Get a reference to the align combo box.
      var align = infoTab.get('cmbAlign');

      align.requiredContent = 'img(align-left,align-right)';

      align.setup = function(type, element) {
        if (type === IMAGE) {
          var value;
          if (element.hasClass('align-left')) {
            value = 'left';
          }
          else if (element.hasClass('align-right')) {
            value = 'right';
          }

          !value && (value = (element.getAttribute('align') || '').toLowerCase());
          this.setValue(value);
        }
      };

      align.commit = function(type, element, internalCommit) {
        var value = this.getValue();
        if (type === IMAGE) {
          element.removeClass('align-' + (value === 'left' ? 'right' : 'left'));
          if (value) {
            element.addClass('align-' + value);
          }
          else {
            element.removeClass('align-' + value);
          }
          if (!internalCommit) {
            value = (element.getAttribute('align') || '').toLowerCase();
            switch (value) {
              // we should remove it only if it matches "left" or "right",
              // otherwise leave it intact.
              case 'left':
              case 'right':
                element.removeAttribute('align');
            }
          }
        } else if (type === PREVIEW) {
          if (value) {
            element.setStyle('float', value);
          }
          else {
            element.removeStyle('float');
          }
        }
        else if (type === CLEANUP) {
          element.removeAttribute('align');
          element.removeStyle('float');
        }
      };


      // Get a reference to the basic vbox.
      var basic = infoTab.get('basic');
      basic.children[0].requiredContent = 'img[width,height]';

      // Get a reference to the width text box.
      var width = infoTab.get('txtWidth');

      width.commit = function(type, element, internalCommit) {
        var value = this.getValue();
        if (type === IMAGE) {
          if (value)
            element.setAttribute('width', value);
          else
            element.removeAttribute('width');
        } else if (type === PREVIEW) {
          var aMatch = value.match(regexGetSize);
          if (!aMatch) {
            var oImageOriginal = this.getDialog().originalElement;
            if (oImageOriginal.getCustomData('isReady') == 'true')
              element.setStyle('width', oImageOriginal.$.width + 'px');
          } else
            element.setStyle('width', CKEDITOR.tools.cssLength(value));
        } else if (type === CLEANUP) {
          element.removeAttribute('width');
          element.removeStyle('width');
        }
      };

      // Get a reference to the height text box.
      var height = infoTab.get('txtHeight');

      height.commit = function(type, element, internalCommit) {
        var value = this.getValue();
        if (type === IMAGE) {
          if (value)
            element.setAttribute('height', value);
          else
            element.removeAttribute('height');
        } else if (type === PREVIEW) {
          var aMatch = value.match(regexGetSize);
          if (!aMatch) {
            var oImageOriginal = this.getDialog().originalElement;
            if (oImageOriginal.getCustomData('isReady') == 'true')
              element.setStyle('height', oImageOriginal.$.height + 'px');
          } else
            element.setStyle('height', CKEDITOR.tools.cssLength(value));
        } else if (type === CLEANUP) {
          element.removeAttribute('height');
          element.removeStyle('height');
        }
      };

    }
  });


  // Overrides image plugin.
  CKEDITOR.plugins.add('ckeditor_image', {
    requires: 'image',
    init: function(editor) {

      CKEDITOR.config.removeDialogTabs = 'image:advanced';

      var image_command = editor.getCommand('image');
      var allowed = 'img[alt,!src,width,height](align-left,align-right)',
              required = 'img[alt,src]';

      image_command.allowedContent = allowed;
      image_command.requiredContent = required;
      image_command.contentTransformations = [
        ['img{width}: sizeToAttribute', 'img[width]: sizeToAttribute'],
        ['img{float}: alignmentToAttribute', 'img[align]: alignmentToAttribute']
      ];

      var image = CKEDITOR.plugins.get('image');

      image.afterInit = function(editor) {
        // Customize the behavior of the alignment commands. (#7430)
        setupAlignCommand('left');
        setupAlignCommand('right');
        setupAlignCommand('center');
        setupAlignCommand('block');

        function setupAlignCommand(value) {
          var command = editor.getCommand('justify' + value);
          if (command) {
            if (value == 'left' || value == 'right') {
              command.on('exec', function(evt) {
                var img = getSelectedImage(editor),
                        align;
                if (img) {
                  align = getImageAlignment(img);
                  if (align == value) {
                    img.removeClass('align-' + value);

                    // Remove "align" attribute when necessary.
                    if (value == getImageAlignment(img))
                      img.removeAttribute('align');

                    // Refresh buttons
                    this.setState(CKEDITOR.TRISTATE_OFF);
                  } else {
                    img.removeClass('align-' + align);
                    img.addClass('align-' + value);

                    // Refresh buttons
                    if (align) {
                      editor.getCommand('justify' + align).setState(CKEDITOR.TRISTATE_OFF);
                    }
                    this.setState(CKEDITOR.TRISTATE_ON);
                  }



                  evt.cancel();
                }
              });
            }

            command.on('refresh', function(evt) {
              var img = getSelectedImage(editor),
                      align;
              if (img) {
                align = getImageAlignment(img);

                this.setState(
                        (align == value) ? CKEDITOR.TRISTATE_ON : (value == 'right' || value == 'left') ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED);

                evt.cancel();
              }
            });
          }
        }
        ;

      };
    }
  });

  function getSelectedImage(editor, element) {
    if (!element) {
      var sel = editor.getSelection();
      element = sel.getSelectedElement();
    }

    if (element && element.is('img') && !element.data('cke-realelement') && !element.isReadOnly())
      return element;
  }

  function getImageAlignment(element) {
    var align;

    if (element.hasClass('align-left')) {
      align = 'left';
    }
    else if (element.hasClass('align-right')) {
      align = 'right';
    }

    if (!align)
      align = element.getAttribute('align');

    return align;
  }

})();
